﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CallCenterOs.Web.Startup))]
namespace CallCenterOs.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
